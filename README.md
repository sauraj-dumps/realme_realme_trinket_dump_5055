## trinket-user 10 QKQ1.200209.002 release-keys
- Manufacturer: realme
- Platform: trinket
- Codename: realme_trinket
- Brand: Realme
- Flavor: syberia_realme_trinket-userdebug
- Release Version: 12
- Id: SQ1D.220105.007
- Incremental: 1644074643
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMX1911/RMX1911:10/QKQ1.200209.002/1608537052:user/release-keys
- OTA version: 
- Branch: trinket-user-10-QKQ1.200209.002-release-keys
- Repo: realme_realme_trinket_dump_5055


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
